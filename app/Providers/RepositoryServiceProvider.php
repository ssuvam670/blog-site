<?php

namespace App\Providers;

use App\Repositories\Implementation\PostRepository;

use App\Repositories\Implementation\UserRepository;
use App\Repositories\Interface\PostRepositoryInterface;
use App\Repositories\Interface\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PostRepositoryInterface::class,PostRepository::class,);
        $this->app->bind(UserRepositoryInterface::class,UserRepository::class,);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
