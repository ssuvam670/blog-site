<?php

namespace App\Repositories\Implementation;


use App\Models\Post;
use App\Repositories\Interface\PostRepositoryInterface;

class PostRepository implements PostRepositoryInterface
{
    public function getAllPost()
    {
        return Post::all();
    }
    public function getPostBySlug($slug){
        return Post::where('slug',$slug)->first();
    }
    public function storePost(array $postDetails){
        return Post::create($postDetails);
    }
}