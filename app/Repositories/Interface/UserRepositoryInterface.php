<?php

namespace App\Repositories\Interface;

interface UserRepositoryInterface 
{

    public function getUserDetails($id);
   
}