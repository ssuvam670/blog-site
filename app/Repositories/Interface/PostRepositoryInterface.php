<?php

namespace App\Repositories\Interface;

interface PostRepositoryInterface 
{

    public function getAllPost();
    public function getPostBySlug($slug);
    public function storePost(array $postDetails);
   
}