<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    public $guarded=[];
    protected $appends=['reading_time'];

    public function getReadingTimeAttribute(){
        $wordsCount = str_word_count($this->description);
        return max(1,round($wordsCount/200)) . " min read";
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
 
}
