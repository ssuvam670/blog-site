<?php

namespace App\Http\Controllers;

use App\Repositories\Interface\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private UserRepositoryInterface $repository;
    public function __construct(UserRepositoryInterface $userRepository) 
    {
        $this->repository = $userRepository;
    }
    public function index($id){
        $data['user']=$this->repository->getUserDetails($id);

        return view('author')->with($data);
    }
}
