
@extends('layout.main')

@section('content')
<div class="container">
<h4>Add your Post</h4>
    <form action="{{route('post.store')}}" method='POST'>
    @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Post Title</label>
    <input type="text" class="form-control" name='title'>
    @error('title')<small class="form-text text-danger">{{$message}}</small>@enderror
  </div>
    <div class="form-group">
    <label for="exampleFormControlFile1">Featured Image</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" name='image'>
     @error('image')<small class="form-text text-danger">{{$message}}</small>@enderror
  </div>

  <div class="form-group">
    <label for="exampleFormControlTextarea1">Description</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name='description'></textarea>
     @error('description')<small class="form-text text-danger">{{$message}}</small>@enderror
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@stop

