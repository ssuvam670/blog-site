<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/',[PostController::class,'index'])->name('home');
Route::get('/post/{slug}',[PostController::class,'singlePost'])->name('post');
Route::get('/user/{id}',[UserController::class,'index'])->name('user');
Route::get('/add/post',[PostController::class,'create'])->middleware('auth')->name('post.add');
Route::post('/add/post',[PostController::class,'store'])->middleware('auth')->name('post.store');
Route::get('/logout',function(){
    
    Auth::logout();
    return redirect()->route('home');
})->middleware('auth')->name('logout');
Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
