<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = Post::class;
    public function definition()
    {
        $ttle=$this->faker->sentence(6,false);
        return [
            'title' => $ttle,
            'slug' =>Str::slug($ttle,'-'),
            'description'=>$this->faker->paragraph(200),
            'image'=>$this->faker->imageUrl(),
            'user_id'=>\App\Models\User::inRandomOrder()->first()['id'],
            'created_at'=>fake()->date()
        ];
    }
}
